CREATE DATABASE dash;

CREATE TABLE `update_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `link` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `job_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `job_link` varchar(100) NOT NULL,
  `job_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state_link` varchar(100) NOT NULL,
  `state_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
